import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder, FormGroupDirective, FormArray} from '@angular/forms';
import { EmpInformation } from '../interface/empInfo.interface';
import {TableComponent} from "../table/table.component";
import {MatRadioGroup} from "@angular/material/radio";
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';


const THUMBUP_ICON = `
  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.` +
  `44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5` +
  `1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-1.91l-.01-.01L23 10z"/>
  </svg>
`;


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  EMP_ELEMENT_DATA: EmpInformation[] = []

  constructor(private formBuilder: FormBuilder, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,) {
    iconRegistry.addSvgIconLiteral('thumbs-up', sanitizer.bypassSecurityTrustHtml(THUMBUP_ICON));
  }

  regisForm!: FormGroup;
  gender: string[] = ['Male', 'Females'];
  typeEmp: string[] = ['Full-Time', 'Path-Time', 'Scholarship']
  @ViewChild(FormGroupDirective) formGroupDirective?: FormGroupDirective;
  isEditing:boolean = false;
  @ViewChild(TableComponent) childTable?:TableComponent;
  @ViewChild(MatRadioGroup) radioGroup?:MatRadioGroup;
  selectedOption: any;
  ngOnInit(): void {
    this.regisForm = this.formBuilder.group({
      id: new FormControl('', []),
      username: new FormControl('', [Validators.required,Validators.pattern("([\\w\\d]){4,16}")]),
      password: new FormControl('', [Validators.required,Validators.pattern("^(?=.*[A-Za-z\\d])(?=.*\\d)[A-Za-z\\d]{8,25}$")]),
      emails: new FormControl('', [Validators.required, Validators.email]),
      gender: new FormControl(this.gender[0],[] ),
      phoneNumber: this.formBuilder.array([]),
      types: new FormControl('Full-Time', []),
      action: new FormControl(null)
    })
  }

  get getPhoneNumberArr(){
    return this.regisForm.get('phoneNumber') as FormArray
  }

  addPhone() {
    const phNumber = this.formBuilder.group({
      pnumber: new FormControl('',[Validators.pattern("^0([\\d]){8,9}")])
    });
    this.getPhoneNumberArr.push(phNumber);
  }
  addEditablePhone(data:string) {
    const phNumber = this.formBuilder.group({
      pnumber: new FormControl(data,[Validators.pattern("^0([\\d]){8,9}")])
    });
    this.getPhoneNumberArr.push(phNumber);
  }
  deletePhone(i: number) {
    return this.getPhoneNumberArr.removeAt(i);
  }



  public onRegister(formDirective:FormGroupDirective) {

    this.regisForm.get('id')?.setValue((Math.random() + 1).toString(36).substring(7))
    if (this.radioGroup?.value){
      this.regisForm.get('gender')?.setValue(this.radioGroup?.value)
    }else {
      this.regisForm.get('gender')?.setValue('Male')
    }
    if (this.selectedOption){
      this.regisForm.get('types')?.setValue(this.selectedOption)
    }else{
      this.regisForm.get('types')?.setValue('Full-Time')
    }
    this.EMP_ELEMENT_DATA.push(this.regisForm.value)
    this.childTable?.table?.renderRows();
    formDirective.resetForm();
    while (this.getPhoneNumberArr.length !== 0) {
      this.getPhoneNumberArr.removeAt(0)
    }

  }

  handleChildEvent(data:EmpInformation[]){
    this.EMP_ELEMENT_DATA = data;
  }

  get getEmailValidate() {
    return this.regisForm.get('emails')
  }

  get getUsernameValidate() {
    return this.regisForm.get('username')
  }

  get getPasswordValidate() {
    return this.regisForm.get('password')
  }
  get getGenderValidate() {
    return this.regisForm.get('gender')
  }

  get getTypesValidate() {
    return this.regisForm.get('types')
  }


  get getRegisForm(){
    return this.regisForm;
  }

  handleUpdateEvent(element: EmpInformation) {
    this.isEditing=true;
    this.regisForm.get('id')?.setValue(element.id);
    this.regisForm.get('username')?.setValue(element.username);
    this.regisForm.get('password')?.setValue(element.password);
    this.regisForm.get('gender')?.setValue(element.gender);
    this.regisForm.get('emails')?.setValue(element.emails);
    this.regisForm.get('types')?.setValue(element.types);
    while (this.getPhoneNumberArr.length !== 0) {
      this.getPhoneNumberArr.removeAt(0)
    }
    element.phoneNumber.forEach((e)=>{
      this.addEditablePhone(e.pnumber);
    })



  }

  onUpdate(formDirective: FormGroupDirective) {
  this.regisForm.get('gender')?.setValue(this.radioGroup?.value)
  this.regisForm.get('types')?.setValue(this.selectedOption)
  this.EMP_ELEMENT_DATA.forEach(data=>{
  if(data.id===this.regisForm.get('id')?.value) {
    this.EMP_ELEMENT_DATA.splice(this.EMP_ELEMENT_DATA.indexOf(data),1,this.regisForm.value);
  }
  })
  this.isEditing=false;
    while (this.getPhoneNumberArr.length !== 0) {
      this.getPhoneNumberArr.removeAt(0)
    }
  formDirective.resetForm();
    this.childTable?.table?.renderRows();
  }

  handleClearForm(isClearing: boolean) {
    this.formGroupDirective?.resetForm();
    this.isEditing=false;
    while (this.getPhoneNumberArr.length !== 0) {
      this.getPhoneNumberArr.removeAt(0)
    }
  }
}
