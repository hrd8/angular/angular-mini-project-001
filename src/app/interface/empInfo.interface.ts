import {EmpPhoneNumber} from "./empPhoneNumber.interface";

export interface EmpInformation {
    image:string;
    id: string;
    username: string;
    password: string;
    emails: string;
    gender:string;
    types:string;
    phoneNumber: EmpPhoneNumber[];
    action:any;

}
