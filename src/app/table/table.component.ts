import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { EmpInformation } from '../interface/empInfo.interface';
import {MatTable} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {ViewDialogComponent} from "../components/view-dialog/view-dialog.component";



@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {

  constructor(public dialog: MatDialog) {}

  newEMP!:any;

  @Input() EMP_ELEMENT_DATA !: EmpInformation[];
  @ViewChild(MatTable) table?: MatTable<any>;
  @Output() deletedData:EventEmitter<EmpInformation[]> = new EventEmitter<EmpInformation[]>();
  @Output() updateData:EventEmitter<EmpInformation> = new EventEmitter<EmpInformation>();
  @Output() clearForm:EventEmitter<boolean> = new EventEmitter<boolean>();
  ngOnInit(): void {
    console.log("emp->> ",this.EMP_ELEMENT_DATA)
    this.newEMP = this.EMP_ELEMENT_DATA;
  }
  public onUpdate(data:EmpInformation){
    this.updateData.emit(data);
  }

  public onDelete(element:EmpInformation) {
    this.clearForm.emit(true)
    this.deletedData.emit( [...this.EMP_ELEMENT_DATA.filter(data => data.id !== element.id)])
    this.table?.renderRows();
  }
  openDialog(element:EmpInformation): void {
    const dialogRef = this.dialog.open(ViewDialogComponent, {
      width: '600px',
      data: element}
    );
    dialogRef.afterClosed().subscribe(result =>console.log(result));
  }

  displayedColumnsEmp: string[] = [
    'id',
    'image',
    'username',
    'password',
    'emails',
    'gender',
    'types',
    'phoneNumber',
    'action',
  ];
  //dataSourceEmp = EMP_ELEMENT_DATA;

}
